const mongoose = require('mongoose');

const companySchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    createDate: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Company', companySchema);