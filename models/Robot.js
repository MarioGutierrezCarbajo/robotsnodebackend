const mongoose = require("mongoose");

const robotSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 4,
        max: 255,
    },
    type: {
        type: String,
        required: true,
        min: 4,
        max: 255,
    },
    company: {
        type: mongoose.Types.ObjectId,
        ref: "Company",
    },
    createDate: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model("Robot", robotSchema);
