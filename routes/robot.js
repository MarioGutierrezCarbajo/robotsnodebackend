const Robot = require("../models/Robot");
const Company = require("../models/Company");

const router = require("express").Router();

router.post("/", async (req, res) => {
    const isNameExist = await Robot.findOne({ name: req.body.name });
    if (isNameExist) {
        return res.status(400).json({ error: "Robot ya registrado" });
    }
    const robot = new Robot({
        name: req.body.name,
        type: req.body.type,
        company: req.body.company,
    });
    try {
        const savedRobot = await robot.save();
        res.json({
            error: null,
            data: savedRobot,
        });
    } catch (error) {
        res.status(400).json({ error });
    }
});

router.get("/", async (req, res) => {
    await Robot.find()
        .populate("company",{_id:0,name:1})
        .then((data) => res.status(200).json(data))
        .catch((err) => {
            res.status.apply(400).json(err);
        });
});

router.put("/:name", async (req, res) => {
    const isNameExist = await Robot.findOne({ name: req.body.name });
    if (isNameExist) {
        return res.status(400).json({ error: "Robot ya registrado" });
    }
    const robot = new Robot({
        name: req.body.name,
        type: req.body.type,
        company: req.body.company,
    });
    try {
        const name = req.params.name
        console.log("name "+name)
        const updatedRobot = await Robot.updateOne(
            { name: req.params.name },
            {
                name: req.body.name,
                type: req.body.type,
                company: req.body.company,
            }
        );
        res.json({
            error: null,
            data: updatedRobot,
        });
    } catch (error) {
        res.status(400).json({ error });
    }
});

router.delete("/:name",async (req, res) => {
    console.log("DELETE zone")
    try {
        const name = req.params.name
        console.log("name "+name)
        const deletedRobot = await Robot.findOneAndRemove(
            { name: req.params.name }
        );
        res.json({
            error: null,
            data: deletedRobot,
        });
    } catch (error) {
        res.status(400).json({ error });
    }
});

module.exports = router;
